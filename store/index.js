import MemoryStream from "memorystream";

//couchdb
const remoteString = 'http://admin:mati123@localhost:5984/paragraphs';

export const state = () => ({
    connected: false,
    users: [],
    username: null,
    socketId: null,

    // webrtc
    channels: {},
    localPeers: {},
    remotePeers: {},
    messages: [],
    totalOnline: 0,
    autoSyncRTC: false,

    //db
    remoteSync: null,
    updateRTCRecords: false
})

export const mutations = {
    disconnect(state) {
        state.connected = false;
    },
    setUsername(state, username) {
        console.log("username", username);
        state.connected = true;
        state.username = username;
    },
    addRemoteUser(state, user) {
        if (state.users.filter(u => u.name === user.name).length === 0 &&
            user.name !== state.username
        ) {
            state.users.push(user);
        }
    },
    logoutUser(state, id) {
        const idx = state.users.findIndex(u => u.id === id);
        state.users.splice(idx, 1);
    },
    initUsers(state, users) {
        state.users = users;
    },
    initSocket(state, socketId) {
        state.socketId = socketId;
    },


    // webrtc
    addRemotePeer(state, data) {
        state.remotePeers[data.from] = data;
    },
    createPeer(state, {username, rtc}) {
        state.localPeers[username] = rtc;
    },
    addMessage(state, message) {
        state.messages.push(message);
    },
    updateRTCConnectionUser(state, { username, isOnline}) {
        const idx = state.users.findIndex(u => u.name === username);
        const user = {...state.users[idx]};
        user.isOnline = isOnline;
        state.users.splice(idx, 1);
        state.users.push(user);

        state.totalOnline = state.users.filter(u => u.isOnline).length;
    },
    updateAutoSync(state, turnOn) {
        state.autoSyncRTC = turnOn;
    },

    //db
    setRemoteSync(state, sync) {
        state.remoteSync = sync;
    },
    setUpdateRTCRecords(state, active) {
        state.updateRTCRecords = active;
    }
}

export const getters = {
    users: state => state.users,
    connected: state => state.connected,
    socketId: state => state.socketId,
    username: state => state.username,
    channels: state => state.channels,
    localPeers: state => state.localPeers,
    remotePeers: state => state.remotePeers,
    messages: state => state.messages,
    remoteSync: state => state.remoteSync,
    totalOnline: state => state.totalOnline,
    autoSyncRTC: state => state.autoSyncRTC,
    updateRTCRecords: state => state.updateRTCRecords
}

export const actions = {
    // sockets
    socket_userconnected({ commit, state, dispatch }, data) {
        console.log("socket_userconnected", data);
        commit("addRemoteUser", data);

        if (!state.username) return;

        state.users.forEach(u => {
            if (state.localPeers[u.name]) return;

            dispatch("createPeerConnection", u.name);
            dispatch("createDataChannel", u.name);
            dispatch("createOffer", u.name);
        });
    },
    socket_login({ commit,dispatch }, data) {
        console.log("socket_login", data);
        commit("initUsers", data.users);
        commit("initSocket", data.id);
        //dispatch("connectDb");
    },
    socket_logout({ commit }, data) {
        console.log("socket_logout", data);
        commit("logoutUser", data);
    },
    socket_signaling({dispatch, state}, data) {
        console.log("socket_signaling", data);

        if (data.to !== state.username) return;

        if (!data.candidate) dispatch("addConnectionPeer", data);
        else dispatch("addCandidate", data);
    },

    //webrtc
    connectToOthers({state, dispatch}) {
        console.log("connectToOthers");

        if (!state.username) {
            console.log("connectToOthers no username");
            return;
        }

        state.users.forEach(u => {
            console.log("connectToOthers", u.name);

            dispatch("createPeerConnection", u.name);
            dispatch("createDataChannel", u.name);
        });
    },
    createPeerConnection({ commit }, username) {
        commit("createPeer", {
            username,
            rtc: new RTCPeerConnection({"iceServers":[]})
        });
    },
    async addConnectionPeer({ commit, state, dispatch }, data) {
        console.log(`${state.username} handle ${data.desc.type} from ${data.from}`, data.desc);

        if (!state.remotePeers[data.from]) {
            commit("addRemotePeer", data);
        }

        await state.localPeers[data.from].setRemoteDescription(data.desc);
        if (data.desc.type === "offer") {
            dispatch("createAnswer", data.from);
        }
        if (data.desc.type === "answer" && data.to === state.username) {
            dispatch("onIceCandidates", data.from);
        }
    },
    async createOffer({ commit, state, dispatch }, username) {
        console.log(`${state.username} create offer to ${username} : start`);
        try {
            const offer = await state.localPeers[username].createOffer({});
            await state.localPeers[username].setLocalDescription(offer);
            console.log(`${state.username}/${username} offer and setLocalDescription: finished`);
            dispatch("sendSignalingMessage", {
                desc: state.localPeers[username].localDescription,
                to: username
            });
        } catch (error) {
            console.log(`Error creating the offer from ${state.username} to ${username}. Error: ${error}`);
        }
    },
    async createAnswer({ commit, state, dispatch }, username) {
        console.log(`${state.username} create an answer to ${username}: start`);
        try {
            const answer = await state.localPeers[username].createAnswer();
            await state.localPeers[username].setLocalDescription(answer);
            console.log(`${state.username}/${username} answer and setLocalDescription: finished`);
            dispatch("sendSignalingMessage", {
                desc: state.localPeers[username].localDescription,
                to: username
            });
        } catch (error) {
            console.log(`Error creating the answer from ${state.username} to ${username}. Error: ${error}`);
        }
    },
    sendSignalingMessage({ state }, { desc, to }) {
        // send the offer to the other peer
        this._vm.$socket.client.emit("signaling", {
            desc,
            from: state.username,
            to
        });
    },
    onIceCandidates({ state,  dispatch}, username) {
        // send any ice candidates to the other peer
        state.localPeers[username].addEventListener('icecandidate', ({ candidate }) => {
            if (!candidate) {
                console.log(state.localPeers[username]);
                dispatch("createOffer", username);
                return;
            }
            this._vm.$socket.client.emit("signaling", {
                candidate,
                from: state.username,
                to: username
            });
        });
        state.localPeers[username].addEventListener('iceconnectionstatechange', e => {
            console.log(`ICE connection update ${username}`, state.localPeers[username].iceConnectionState);
            if (state.localPeers[username].iceConnectionState=== "disconnected") {
                dispatch("createOffer", username);
            }
        });
    },
    async addCandidate({ state }, candidate) {
        console.log("addcandidate", candidate);

        try {
            if (!state.localPeers[candidate.from]) return;
            await state.localPeers[candidate.from].addIceCandidate(candidate.candidate);
        } catch (error) {
            console.log(`Error adding a candidate in ${candidate.from}. Error: ${error}`)
        }
    },
    createDataChannel({ state, dispatch,commit }, username) {
        console.log("create data channel", username);

        state.channels[username] = state.localPeers[username].createDataChannel("tlChannel");
        state.channels[username].addEventListener('error', (event) => {
            console.log(`${username} error`, error);
        });
        state.channels[username].addEventListener('open', () => {
            console.log(`${username} connection opened`);
            commit("updateRTCConnectionUser", {
                username,
                isOnline: true
            });
        });
        state.channels[username].addEventListener('close', () => {
            console.log(`${username} connection closed`);
            commit("updateRTCConnectionUser", {
                username,
                isOnline: false
            });
        });
        state.channels[username].addEventListener('message', (e) => {
            if (e.data.indexOf("chat:") === 0) {
                dispatch("addChatMessage", e.data.substr(5, e.data.length));
            } else if (e.data.indexOf("doc:") === 0) {
                dispatch("importRemoteRecord", JSON.parse(e.data.substr(4, e.data.length)));
            } else {
                dispatch("importRemoteDb", e.data);
            }
        });
        state.localPeers[username].ondatachannel = event => state.channels[username] = event.channel;
    },
    addChatMessage({commit}, msg) {
        commit("addMessage", msg);
    },


    //db
    connectDb({commit, dispatch}) {
        const sync = this._vm.$pouch.sync('paragraphs', remoteString, {
            live: true,
            retry: true
        }).on('change', function (info) {
            // handle change
            console.log("pouch change", info);
        }).on('paused', function (err) {
            // replication paused (e.g. replication up to date, user went offline)
            console.log("pouch replication paused", err);
        }).on('active', function () {
            // replicate resumed (e.g. new changes replicating, user went back online)
            console.log("pouch replication resumed");
        }).on('denied', function (err) {
            // a document failed to replicate (e.g. due to permissions)
            console.log("pouch failed to replicate", err);
        }).on('complete', function (info) {
            console.log("pouch replication complete");
            // handle complete
        }).on('error', function (err) {
            // handle error
            console.log("pouch error", err);
        });

        this._vm.$on("pouchdb-db-created", () => {
            commit("setRemoteSync", true);
        });
    },
    disconnectDb({commit}) {
        commit("setRemoteSync", false);
        this._vm.$pouch.close(remoteString);
    },
    syncRTCDb({state}, toUsers) {
        // send my local pouchdb to remote user
        const stream = new MemoryStream();
        let dumpedString = '';
        stream.on('data', function(chunk) {
            dumpedString += chunk.toString();
        });
        this._vm.$databases['paragraphs'].dump(stream, {revs_: true}).then(function () {
            toUsers.forEach(toUser => {
                state.channels[toUser].send(dumpedString);
            });
        }).catch(function (err) {
            console.log('oh no an error', err);
        });
    },
    syncRTCRecord({state}, { toUsers, doc }) {
        toUsers.forEach(toUser => {
            state.channels[toUser].send(`doc:${JSON.stringify(doc)}`);
        });
    },
    importRemoteDb({}, db) {
        const self = this;
        this._vm.$databases.paragraphs.loadIt(db).then(() => {

        });
    },
    importRemoteRecord({}, newDoc) {
        this._vm.$databases.paragraphs.upsert(newDoc._id, doc => {
            doc.text= newDoc.text;
            doc.timestamp= newDoc.timestamp;
            doc.updated= newDoc.updated;
            return doc;
        });
    },
    autoSyncRTC({ commit }, on) {
        commit("updateAutoSync", on);
    },
    switchUpdateRTCRecords({ commit }, on) {
        commit("setUpdateRTCRecords", on);
    }
}