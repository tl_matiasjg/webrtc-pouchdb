# tinker-webrtc

> A simple CouchDb and PouchDb test with WebRTC included

## Build Setup

1- Go to the Socket dir (`/socket-server`) and run the server:

$ node index.js 

2- Go to the App dir (`/tinker-webrtc`) and compile and run the client:

$ npm run dev