import Vue from 'vue'
import VueSocketIOExt from 'vue-socket.io-extended'
import io from 'socket.io-client';

export default ({ store }) => {
  const socket = io('http://localhost:4000');

  Vue.use(VueSocketIOExt, socket, { store });
}
