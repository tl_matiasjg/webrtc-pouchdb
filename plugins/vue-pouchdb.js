import Vue from 'vue'

import PouchDB from 'pouchdb-browser'
import PouchFind from 'pouchdb-find'
import PouchLiveFind from 'pouchdb-live-find'
import PouchVue from 'pouch-vue';
import replicationStream from 'pouchdb-replication-stream';
import PouchLoad from 'pouchdb-load';
import PouchUpsert from 'pouchdb-upsert';

PouchDB.plugin({
  loadIt: PouchLoad.load
});
PouchDB.plugin(PouchFind);
PouchDB.plugin(PouchLiveFind);
PouchDB.plugin(PouchUpsert);
PouchDB.plugin(replicationStream.plugin);
PouchDB.adapter('writableStream', replicationStream.adapters.writableStream);

Vue.use(PouchVue, {
    pouch: PouchDB,
    defaultDB: 'paragraphs',
    optionsDB: {
        fetch: function (url, opts) {
            opts.credentials = 'include';
            return PouchDB.fetch(url, opts);
        }
    }
});


  